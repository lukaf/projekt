package server

import (
	"context"

	pg "github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"

	"example.com/protobuf"
)

type Record struct {
	Ocid string `pg:",notnull"`
	Data string `pg:"type:json,notnull"`
}

func NewRecord(r *protobuf.Request) *Record {
	return &Record{
		Ocid: r.GetOcid(),
		Data: string(r.GetData()),
	}
}

type Db struct {
	client Database
	ctx    context.Context
}

type Database interface {
	Ping(context.Context) error
	Model(...interface{}) *orm.Query
	Close() error
}

func NewDb(options *pg.Options) (*Db, error) {
	db := &Db{
		client: pg.Connect(options),
		ctx:    context.Background(),
	}

	if err := db.ping(); err != nil {
		return nil, err
	}

	return db, nil
}

func (db *Db) ping() error {
	return db.client.Ping(db.ctx)
}

func (db *Db) insert(data *Record) error {
	_, err := db.client.Model(data).Insert()
	return err
}

func (db *Db) upsert(data *Record) error {
	_, err := db.client.Model(data).
		OnConflict("(ocid) DO UPDATE").
		Set("data = EXCLUDED.data").
		Insert()
	return err
}

func (db *Db) close() {
	db.client.Close()
}

func (db *Db) Insert(ctx context.Context, r *protobuf.Request) (*protobuf.Response, error) {
	record := NewRecord(r)

	err := db.insert(record)

	response := &protobuf.Response{
		Error: err != nil,
	}

	if err != nil {
		response.Message = err.Error()
	}

	return response, err
}

func (db *Db) Upsert(ctx context.Context, r *protobuf.Request) (*protobuf.Response, error) {
	record := NewRecord(r)

	err := db.upsert(record)

	response := &protobuf.Response{
		Error: err != nil,
	}

	if err != nil {
		response.Message = err.Error()
	}

	return response, err
}
