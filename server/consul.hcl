job "consul" {
  datacenters = ["dc1"]

  type = "system"

  group "consul" {
    task "consul" {
      driver = "docker"

      config {
        image = "consul:latest"
      }

      resources {
        network {
          port http {}
          port grpc {}
        }
      }
    }
  }
}
