package server

import (
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"example.com/protobuf"
	pg "github.com/go-pg/pg/v10"
	grpc "google.golang.org/grpc"
)

var flagset *flag.FlagSet = flag.NewFlagSet("server", flag.ExitOnError)

var (
	addr   = flagset.String("addr", ":8080", "")
	dbHost = flagset.String("db-host", "127.0.0.1", "DB Host")
	dbPort = flagset.Int("db-port", 5432, "DB port")
	dbUser = flagset.String("db-user", "postgres", "DB Username")
	dbPass = flagset.String("db-pass", "root", "DB Password")
	dbDb   = flagset.String("db-db", "records", "DB Database")
)

func NewDbConnection(dbOpts *pg.Options) *Db {
	var db *Db
	var err error

	go func() {
		backoff := 1

		for {
			db, err = NewDb(dbOpts)
			if err == nil {
				return
			}

			log.Println("db error: " + err.Error())

			if backoff < 10 {
				backoff += 1
			}
			time.Sleep(time.Second * time.Duration(backoff))
		}
	}()

	return db
}

func Run(args []string) error {
	if err := flagset.Parse(args); err != nil {
		return err
	}

	db := NewDbConnection(
		&pg.Options{
			Addr:     fmt.Sprintf("%s:%d", *dbHost, *dbPort),
			User:     *dbUser,
			Password: *dbPass,
			Database: *dbDb,
		},
	)

	l, err := net.Listen("tcp", *addr)
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()

	protobuf.RegisterRecordServer(grpcServer, db)
	return grpcServer.Serve(l)
}
