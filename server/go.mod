module example.com/server

go 1.14

require (
	example.com/protobuf v0.0.0
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.0.0-beta.8
	google.golang.org/grpc v1.30.0
)

replace example.com/protobuf => ../protobuf
