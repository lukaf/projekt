job "database" {
  datacenters = ["dc1"]

  type = "service"

  group "sql" {

    ephemeral_disk {
      sticky = true
    }

    task "postgres" {
      driver = "docker"

      config {
        image = "postgres:alpine"

        volumes = [
          "/opt/nomad/storage/database:/var/lib/postgresql/data",
        ]
      }

      env {
        POSTGRES_PASSWORD = "root"
      }

      resources {
        network {
          port  "db"  {
            static = 5432
          }
        }
      }
      service {
        port = "db"

        check {
          type = "tcp"
          port = "db"
          interval = "30s"
          timeout = "5s"
        }
      }
    }
  }
}
