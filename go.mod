module projekt

go 1.14

require (
	example.com/client v0.0.0
	example.com/protobuf v0.0.0
	example.com/server v0.0.0
	github.com/go-pg/pg/v10 v10.0.0-beta.8
	github.com/golang/protobuf v1.4.2
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.25.0
)

replace (
	example.com/client => ./client
	example.com/protobuf => ./protobuf
	example.com/server => ./server
)
