job "consul" {
  datacenters = ["dc1"]

  type = "system"

  group "consul" {
    task "consul" {
      driver = "docker"
      config {
        image = "consul:latest"
        args = [
          "agent",
          "-server",
          "-bootstrap-expect", "2",
          "-ui",
          "-bind", "${attr.unique.network.ip-address}",
          "-advertise", "${attr.unique.network.ip-address}",
          "-client", "0.0.0.0",
          "-join", "192.168.1.204",
          "-join", "192.168.1.205",
          "-disable-host-node-id",
          "-node", "${NOMAD_ALLOC_ID}"
        ]
        network_mode = "host"
      }
    }
  }
}
