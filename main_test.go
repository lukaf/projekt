package main

import (
	"testing"
)

func TestValidateSubcommand(t *testing.T) {
	var got *arguments
	var err error

	_, err = validateSubcommand([]string{})
	if err == nil {
		t.Errorf("passing no arguments should return an error")
	}

	_, err = validateSubcommand([]string{"exe"})
	if err == nil {
		t.Errorf("passing a single argument should return an error")
	}

	_, err = validateSubcommand([]string{"exe", "subcommand"})
	if err == nil {
		t.Errorf("passing an invalid subcommand should return an error")
	}

	got, err = validateSubcommand([]string{"exe", "server"})
	if err != nil {
		t.Errorf("passing a valid subcommand should not return an error")
	}
	if got == nil {
		t.Errorf("passing a valid subcommand should not return nil")
	}

	got, _ = validateSubcommand([]string{"exe", "server", "option"})
	if got.subcommand != "server" {
		t.Errorf("invalid structure value, expected subdcommand 'server', got '%s'", got.subcommand)
	}
	if len(got.options) != 1 || got.options[0] != "option" {
		t.Errorf("invalid structure value, expected options 'option', got '%s'", got.options)
	}

	got, _ = validateSubcommand([]string{"exe", "server", "one", "two", "three"})
	if len(got.options) != 3 {
		t.Errorf("expected to get 3 options in structure, got %d", len(got.options))
	}
}
