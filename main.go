package main

import (
	"fmt"
	"log"
	"os"

	"example.com/client"
	_ "example.com/client"
	"example.com/server"
)

type arguments struct {
	subcommand string
	options    []string
}

func validateSubcommand(args []string) (*arguments, error) {
	if len(args) == 0 || len(args) == 1 {
		return nil, fmt.Errorf("not enough arguments passed")
	}

	switch args[1] {
	case "server":
		return &arguments{
			subcommand: "server",
			options:    args[2:],
		}, nil
	case "client":
		return &arguments{
			subcommand: "client",
			options:    args[2:],
		}, nil
	default:
		return nil, fmt.Errorf("unknown subcommand: %s", args[1])
	}
}

func main() {
	args, err := validateSubcommand(os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		fmt.Fprintf(os.Stderr, "Possible subcommands: server, client\n")
		os.Exit(1)
	}

	if args.subcommand == "server" {
		log.Fatal(server.Run(args.options))
	}

	if args.subcommand == "client" {
		log.Fatal(client.Run(args.options))
	}
}
