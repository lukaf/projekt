package client

import "testing"

func TestIsLastPage(t *testing.T) {
	tests := []struct {
		description string
		page        int
		maxPage     int
		expect      bool
	}{
		{
			description: "Not on the last page",
			page:        10,
			maxPage:     100,
			expect:      false,
		},
		{
			description: "On the last page",
			page:        10,
			maxPage:     10,
			expect:      true,
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			notice := &Notice{
				MaxPage: test.maxPage,
			}

			if test.expect != notice.IsLastPage(test.page) {
				t.Fatal("IsLastPage returned unexpected value")
			}
		})
	}
}
