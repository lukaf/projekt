package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Notice struct {
	MaxPage   int      `json:"maxPage"`
	HitsCount int      `json:"hitsCount"`
	Results   []Result `json:"results"`
}

type Result struct {
	Releases []Release `json:"releases"`
}

type Release struct {
	OCID string `json:"ocid"`
}

func (n *Notice) IsLastPage(page int) bool {
	return page == n.MaxPage
}

func getNoticePage(page int) (*Notice, error) {
	url := fmt.Sprintf("%s?page=%d&size=1", noticeUrl, page)
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch notice page %d: %w", page, err)
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to read body of notice page %d: %w", page, err)
	}

	var notice *Notice
	if err := json.Unmarshal(data, &notice); err != nil {
		return nil, fmt.Errorf("unable to unmarshal notice page %d: %w", page, err)
	}

	return notice, nil
}

func getAllNoticeOcidsFromPage(page int) chan string {
	ch := make(chan string)

	go func() {
		for {
			notice, err := getNoticePage(page)
			if err != nil {
				fmt.Println(err)
				continue
			}

			for _, result := range notice.Results {
				for _, release := range result.Releases {
					ch <- release.OCID
				}
			}

			if !notice.IsLastPage(page) {
				page += 1
			} else {
				break
			}
		}
	}()

	return ch
}
