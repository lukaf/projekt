module example.com/client

go 1.14

require (
	google.golang.org/grpc v1.31.0
	example.com/protobuf v0.0.0
)

replace example.com/protobuf => ../protobuf
	 
