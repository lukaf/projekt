package client

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func getRecord(ocid string) ([]byte, error) {
	url := fmt.Sprintf("%s/%s", recordUrl, ocid)

	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch record with ocid %s: %w", ocid, err)
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to read body of record with ocid %s: %w", ocid, err)
	}

	return data, nil
}
