package client

import (
	"context"
	"flag"
	"fmt"
	"os"

	"example.com/protobuf"
	"google.golang.org/grpc"
)

const (
	noticeUrl = "https://www.contractsfinder.service.gov.uk/Published/Notices/OCDS/Search"
	recordUrl = "https://www.contractsfinder.service.gov.uk/Published/OCDS/Record"
)

type Client struct{}

var flagset *flag.FlagSet = flag.NewFlagSet("client", flag.ExitOnError)

var (
	serverIp   = flagset.String("server-ip", "127.0.0.1", "IP of a record server")
	serverPort = flagset.Int("server-port", 8080, "Port of a record server")
	page       = flagset.Int("page", 1, "Start page (notices)")
	daily      = flagset.Bool("daily", false, "Fetch daily notices")
)

func insertRecordsFromPage(client protobuf.RecordClient, page int) {
	ch := getAllNoticeOcidsFromPage(page)

	ctx := context.Background()

	for ocid := range ch {
		record, err := getRecord(ocid)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}

		_, err = client.Insert(ctx, &protobuf.Request{
			Ocid: ocid,
			Data: record,
		})
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}
}

func Run(args []string) error {
	if err := flagset.Parse(args); err != nil {
		return err
	}

	serverAddr := fmt.Sprintf("%s:%d", *serverIp, *serverPort)
	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())
	if err != nil {
		return err
	}

	recordClient := protobuf.NewRecordClient(conn)

	insertRecordsFromPage(recordClient, *page)

	return nil

	// conn, err := grpc.Dial(":8080", grpc.WithInsecure())
	// if err != nil {
	// 	return err
	// }

	// client := NewRecordClient(conn)

	// ctx := context.Background()

	// {
	// 	fmt.Println("insert")
	// 	response, err := client.Insert(ctx, &Request{
	// 		Ocid: "123-123-123",
	// 		Data: []byte(`{"hello": "world"}`),
	// 	})

	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}

	// 	fmt.Println(response)
	// }

	// {
	// 	fmt.Println("upsert")
	// 	response, err := client.Upsert(ctx, &Request{
	// 		Ocid: "123-123-123",
	// 		Data: []byte(`{"hello": "there"}`),
	// 	})

	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}

	// 	fmt.Println(response)
	// }

	// return nil
}
